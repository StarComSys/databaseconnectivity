﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseConnectivity
{
	public static class DBSUtilities
	{
		public static string BuildConString()
		{
			string ip = "";
			try
			{
				// get the file from @"C:\vfw\connect.txt"
				using (StreamReader sr = new StreamReader(@"C:\vfw\connect.txt"))
				{
					ip = sr.ReadLine();

					// Check to see if the ip address has a value
					// If not, the use localhost
					if (string.IsNullOrWhiteSpace(ip)) {
						ip = "localhost";
					}
				}

				string conString = $"Server={ip};";
				conString += "uid=dbsAdmin;";
				conString += "pwd=password;";
				conString += "database=ContactList;";
				conString += "port=8889;";
				conString += "SslMode=none;";

				return conString;
			}
			catch (Exception)
			{
				MessageBox.Show("Error trying to build SQL connection string!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			return null;
		}

		public static MySqlConnection Connect(string connectionString)
		{
			// Attepmt to connection to the MySQL server
			MySqlConnection _con = new MySqlConnection();

			try
			{
				_con.ConnectionString = connectionString;
				_con.Open();
				MessageBox.Show("Connected!");
			}
			catch (MySqlException ex)
			{
				// need a variable for the message
				string msg = "";

				// Get the error
				switch (ex.Number)
				{
					case 0:
						msg = ex.ToString();
						break;
					case 1042:
						msg = "Cannot resolve host address.\n";
						break;
					case 1045:
						msg = "Invalid username/password";
						break;
					default:
						msg = ex.ToString();
						break;
				}

				MessageBox.Show(msg);
			}

			return _con;
		}

		public static DataTable GetData(MySqlConnection sqlConn)
		{
			// Get between 5 and 15 contacts
			Random rnd = new Random();
			int numContacts = rnd.Next(5, 16);  // creates a number between 5 and 15

			// Let's get the data
			string sqlQuery = $"SELECT Id, FirstName, LastName, PhoneNumber, Email, Relation FROM ContactList.MyContacts LIMIT 15;"; //{numContacts};";
			MySqlDataAdapter adapter = new MySqlDataAdapter(sqlQuery, sqlConn);
			DataTable data = new DataTable();

			adapter.SelectCommand.CommandType = CommandType.Text;
			adapter.Fill(data);

			return data;
		}
	}
}
