﻿using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace DatabaseConnectivity
{
	public partial class Form1 : Form
	{
		// public global connection for the server
		MySqlConnection conn = new MySqlConnection();

		// Datatable to hold data
		DataTable data = new DataTable();

		int recordIndex = 0;

		public Form1()
		{
			InitializeComponent();

			// Build Connection String
			string connString = DBSUtilities.BuildConString();

			// Connect to the server
			conn = DBSUtilities.Connect(connString);

			// Get the data
			data = DBSUtilities.GetData(conn);


		}

		
	}
}
